#include "mainwindow.h"
#include <iostream>
#include <QMouseEvent>
#include <cmath>
#include "animator.h"
#include <QThread>

MainWindow::MainWindow(QWidget *parent)
    : QGLWidget(parent)
{

    glMatrixMode(GL_MODELVIEW);
    glLoadIdentity();
    glPushMatrix();

    this->resize(1000,800);

    int n = 100, m = 100;
    net = Net2D(n,m);
    for(int i=0; i<n; i++)
    {
        for(int j=0; j<m; j++)
        {
            net(i,j).alive = (std::rand()%2);
        }
    }

    vertice_plane();
//    vertice_sphere();



}

void MainWindow::vertice_plane()
{
    vert.clear();
    col.clear();
    ind.clear();
    int n = net.H, m = net.W;

    GLfloat step = 1.0/n;
    GLfloat X0 = -n*step/2,
            Y0 = -m*step/2;
    GLuint index = 0;
    for(int i=0; i<n; i++)
        for(int j=0; j<m; j++)
        {
            auto cur_cell = net(i,j);
            GLfloat c = (cur_cell.alive) ? 1.0 : 0.0;
            QVector<GLfloat> color(12,c);

            GLfloat cur_X0 = X0+i*step,
                    cur_Y0 = Y0+j*step;

            vert.append({cur_X0,cur_Y0,0});
            vert.append({cur_X0+step ,cur_Y0,0});
            vert.append({cur_X0+step,cur_Y0+step,0});
            vert.append({cur_X0,cur_Y0+step,0});

            col.append(color);

            for(int k=0; k<4; k++)
            {
               ind.push_back(index);
               index += 1;
            }



        }
}
void MainWindow::vertice_sphere()
{
    vert.clear();
    col.clear();
    ind.clear();
    int n = net.H, m = net.W;

    GLfloat step = 1.0/n;
    float step_phi = 2*M_PI*step,
          step_theta = M_PI*step;
    GLfloat X0 = 0,
            Y0 = 0;

    GLuint index = 0;
    for(int i=0; i<n; i++)
        for(int j=0; j<m; j++)
        {
            auto cur_cell = net(i,j);
            GLfloat c = (cur_cell.alive) ? 1.0 : 0.0;
            QVector<GLfloat> color(12,c);

            float cur_phi = 2*M_PI*(X0+i*step),
                  cur_theta = M_PI*(Y0+j*step);

            GLfloat st = sin(cur_theta),
                    sp = sin(cur_phi),
                    cp = cos(cur_phi);
            GLfloat x1 = cp * st,
                    y1 = sp * st,
                    z1 = cos(cur_theta);
            vert.append({x1,y1,z1});

            GLfloat c2 = cos(cur_phi+step_phi),
                    s2 = sin(cur_phi+step_phi);
            GLfloat x2 = c2 * sin(cur_theta),
                    y2 = s2 * sin(cur_theta);
            vert.append({x2,y2,z1});

            GLfloat st_ = sin(cur_theta+step_theta);
            GLfloat x3 = c2 * st_,
                    y3 = s2 * st_,
                    z3 = cos(cur_theta+step_theta);
            vert.append({x3,y3,z3});

            GLfloat x4 = cp * st_,
                    y4 = sp * st_;
            vert.append({x4,y4,z3});

            col.append(color);

            for(int k=0; k<4; k++)
            {
               ind.push_back(index);
               index += 1;
            }



        }
}

MainWindow::~MainWindow()
{
}

void MainWindow::runAnimation()
{
    Animator * worker = new Animator();
    QThread * thread = new QThread();

    connect(worker, &Animator::updateSignal,
            this, &MainWindow::updateFrame);
    connect(thread, &QThread::finished,
            worker, &QObject::deleteLater);
    connect(thread, &QThread::started,
            worker, &Animator::doWork);

    worker->moveToThread(thread);
    thread->start();
}

void MainWindow::initializeGL()
{
    initializeOpenGLFunctions();
    glEnable(GL_DEPTH_TEST);
    glClearColor(0.5,0.5,0.5,1);

}

void MainWindow::resizeGL(int w, int h)
{
    glMatrixMode(GL_PROJECTION);
    glLoadIdentity();

    GLfloat d = 1;
    GLfloat xa = -d, xb = d, ya = -d, yb = d, za = -d, zb = d;

    GLfloat ratio = (GLfloat)h/(GLfloat)w;
    if(w>=h)
        glOrtho(xa/ratio, xb/ratio, ya, yb, za, zb);
    else glOrtho(xa, xb, ya*ratio, yb*ratio, za, zb);

    glViewport(0, 0, (GLint)w, (GLint)h);

}



void MainWindow::paintGL()
{
    col.clear();
    QVector<GLfloat> white(12,1.0),
                     black(12,0.0);
    int n = net.H,
        m = net.W;
    for(int i=0; i<n; i++)
        for(int j=0; j<m; j++)
        {
            auto cur_cell = net(i,j);
            if(cur_cell.alive)
                col.append(white);
            else
                col.append(black);
        }

    glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);

    glLineWidth( 5 );

    glMatrixMode(GL_MODELVIEW);
    glPopMatrix();

    glEnableClientState(GL_VERTEX_ARRAY);
    glEnableClientState(GL_COLOR_ARRAY);

    glVertexPointer(3, GL_FLOAT, 0, vert.begin());
    glColorPointer(3, GL_FLOAT, 0, col.begin());

    glDrawElements(GL_QUADS, ind.size(), GL_UNSIGNED_INT, ind.begin());

    glDisableClientState(GL_VERTEX_ARRAY);
    glDisableClientState(GL_COLOR_ARRAY);

    glPushMatrix();

}

void MainWindow::updateFrame()
{

    QVector <std::pair<int,int>> d = {{-1,-1},  {0, -1},  {1, -1}, {-1,0}, {1,0}, {-1,1}, {0,1}, {1,1}} ;

    Net2D updNet(net.H,net.W);

    for(int i=0; i<net.H; i++)
        for(int j=0; j<net.W; j++)
        {
            int an = 0; // number of alive neighbours
            for(int k=0; k<8; k++)
            {
                if( net(i+d[k].first,j+d[k].second).alive )
                    an++;
            }

            if( !net(i,j).alive && (an==3) )
            {
                updNet(i,j).alive = true;
            }
            else if(net(i,j).alive && (an<2 || an>3)){
                updNet(i,j).alive = false;
            }
            else if(net(i,j).alive && (an>1 && an<4)){
                updNet(i,j).alive = true;
            }
        }
    net._cells = updNet._cells;

    updateGL();
}


void MainWindow::mousePressEvent(QMouseEvent* pe){
    mPos = pe->pos();

}
void MainWindow::mouseMoveEvent(QMouseEvent* pe){

    auto mMove = (pe->pos() - mPos);
    float angle = 0.3 * mMove.manhattanLength();

    glMatrixMode(GL_MODELVIEW);
    glPopMatrix();
        glRotatef(angle,(GLfloat)-mMove.y(),(GLfloat)mMove.x(),0);
    glPushMatrix();

    mPos = pe->pos();
    updateGL();
}
void MainWindow::mouseReleaseEvent(QMouseEvent *pe){
    mPos = pe->pos();
}


