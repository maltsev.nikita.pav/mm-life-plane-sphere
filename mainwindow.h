#ifndef MAINWINDOW_H
#define MAINWINDOW_H

#include <QMainWindow>
#include <QGLWidget>
#include <QOpenGLFunctions>

#include "net2d.h"

class MainWindow : public QGLWidget, protected QOpenGLFunctions
{
    Q_OBJECT

public:
    MainWindow(QWidget *parent = nullptr);
    ~MainWindow();

    void runAnimation();

    Net2D net;

protected:

    QPoint mPos;

    void initializeGL() override;
    void resizeGL(int w, int h) override;
    void paintGL() override;
    void vertice_plane ();
    void vertice_sphere ();

    virtual void mousePressEvent(QMouseEvent* pe) override;   // методы обработки события мыши при нажатии клавиши мыши
    virtual void mouseMoveEvent(QMouseEvent* pe) override;    // методы обработки события мыши при перемещении мыши
    virtual void mouseReleaseEvent(QMouseEvent* pe) override; // методы обработки событий мыши при отжатии клавиши мыши
//    virtual void wheelEvent(QWheelEvent* pe) override;

    QVector<GLfloat> vert, col;
    QVector<GLuint> ind;

public slots:
    void updateFrame();

};
#endif // MAINWINDOW_H
