#include "net2d.h"

GridCell::GridCell()
    : alive(false)  {}

GridCell GridCell::operator=(const GridCell &oth)
{

    return GridCell(oth);
}

Net2D::Net2D(int n, int m)
{
    H = n; W = m;
    _cells = vvecGC(H,vecGC(W,GridCell()));
}

GridCell& Net2D::operator()(int i, int j)
{
    return _cells[(i+H) % H][(j+W) % W];
}
