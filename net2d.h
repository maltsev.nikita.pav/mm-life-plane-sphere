#ifndef NET2D_H
#define NET2D_H

#include <QVector>

class GridCell
{
public:
    GridCell();
    float r,g,b;
    bool alive;

    GridCell operator = (const GridCell &oth);
};

using vecGC = QVector<GridCell>;
using vvecGC = QVector<vecGC>;

class Net2D
{
public:
    Net2D(int n=10, int m=10);

    int H,W;
    vvecGC _cells;

    GridCell& operator()(int i, int j);
};

#endif // NET2D_H
